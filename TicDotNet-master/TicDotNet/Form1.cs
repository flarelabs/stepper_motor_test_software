﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using Microsoft.VisualBasic;
using System.Reflection;
using System.Diagnostics;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        static tic m_tic;
        static float resetPin;
        static float[] sensorValue = { 0.0f, 0.0f };
        static bool m_conected = false;
        bool m_moving = false;
        bool m_homing = false;
        bool m_jogging = false;
        static bool exitMeasurements = false;
        static int newSpeedValue = 0;
        static int stepPerMM = 0;
        static bool stepDirectionX;
        static bool stepDirectionY;
        static string serialXStr;
        static string serialYStr;
        private bool m_decelerating;
        int sampleCNT = 0;
        float[,] sample2 = new float[50, 2];
        static int timerCNT;
        static Form inputBox = new Form();
        static System.Windows.Forms.Label lblSerialY = new Label();
        static System.Windows.Forms.Label lblSerialX = new Label();
        float nextPosition = 0.0f;
        static float startTest = (float)0.5f;
        static float endTest = (float)0.5f;
        static float stepTest = (float)0.5;
        static float listA;
        static float listB;
        static float[] calNumbers = { 0, 0 };
        static int lastAxis = 0;
        public byte pinStates;
        float[] sensorConvert = new float[50] {
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            };
        float[,] newCal = new float[,] {
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
              {0, 0},
            };
        public Form1()
        {
            InitializeComponent();
            buttonGoto.Enabled = m_conected;
            buttonHome.Enabled = m_conected;
            checkBoxJogLeft.Enabled = m_conected;
            checkBoxJogRigth.Enabled = m_conected;
            buttonEnergize.Enabled = m_conected;
            m_tic = new tic();
            using (var reader = new StreamReader(@"CalNumbers.csv"))
            {
                int floatCNT = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var listA = line.Split(',');

                    newCal[floatCNT, 0] = float.Parse(listA[0]);
                    newCal[floatCNT, 1] = float.Parse(listA[1]);
                    floatCNT++;
                }
            }

            using (var reader = new StreamReader(@"sensorTable.csv"))
            {
                int floatCNT = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var listA = line.Split(',');

                    sensorConvert[floatCNT] = float.Parse(listA[0]);
                    floatCNT++;
                }
            }
        }

        private void bn_Conect_Click(object sender, EventArgs e)
        {

            try
            {
                if (!m_conected)
                {

                    ; m_conected = m_tic.open(tic.PRODUCT_ID.T36V4, "00377993");
                    m_tic.get_variables();
                    m_tic.reinitialize();
                    //                    m_tic.reset();
                    m_tic.energize();
                    m_tic.clear_driver_error();

                    //m_tic.set_max_accel(100000);
                    //m_tic.set_max_decel(100000);
                    //m_tic.set_max_speed(50000000);
                    //m_tic.set_starting_speed(2000000);
                    m_tic.exit_safe_start();
                    m_tic.wait_for_device_ready();
           

                }
                else
                {
                    m_tic.deenergize();
                    m_tic.close();
                  
                    m_conected = false;
                }

                buttonGoto.Enabled = m_conected;
                buttonHome.Enabled = m_conected;
                checkBoxJogLeft.Enabled = m_conected;
                checkBoxJogRigth.Enabled = m_conected;
                buttonEnergize.Enabled = m_conected;
                RESET_POS.Enabled = m_conected;
                cmdJog.Enabled = m_conected;
                cmdJogUp.Enabled = m_conected;
                m_tic.halt_and_set_position(100);

            }
            catch (Exception ex)
            {
                m_conected = false;
                MessageBox.Show(ex.Message);
            }
        }
        private void runToPosition(int Axis = 0, float target = (float)0.0, bool Reset = false)
        {
            string serial;
            try
            {
                if (!m_conected)
                {
                    if (Axis == 0)
                    {
                        serial = serialXStr;
                    }
                    else
                    {
                        serial = serialYStr;
                     }
                    m_conected = m_tic.open(tic.PRODUCT_ID.T36V4, serial);
                    m_tic.get_variables();
                    m_tic.reinitialize();
                    //                    m_tic.reset();
                    m_tic.energize();
                    m_tic.clear_driver_error();
                    
                        m_tic.set_max_speed(newSpeedValue);
                     m_tic.set_max_accel(100000);
                     m_tic.set_max_decel(100000);
                   
                     m_tic.set_starting_speed(1000);
                    
                    m_tic.exit_safe_start();    
                    m_tic.wait_for_device_ready();
                    m_tic.exit_safe_start();
                    m_tic.set_scl_config(0x0000);

                    buttonGoto.Enabled = m_conected;
                }
                else
                {
                    m_tic.deenergize();
                    m_tic.close();

                    m_conected = false;
                }


                long UtcTime;
                UtcTime = DateTime.UtcNow.Ticks;

                Delay(2);
                runMotor(Axis, target, Reset);
                if (Reset)
                {
                    m_tic.halt_and_set_position(0);
                }
            }
            catch (Exception ex)
            {
                m_conected = false;
                MessageBox.Show(ex.Message);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {

            Int32 tmpPosition;
            float tmpFloatPosition;
            if ( timerCNT > 0 )
            {
                timerCNT--;
            }
            if (m_conected)
            {
                m_tic.reset_command_timeout();
                m_tic.get_variables();
                m_tic.get_status_variables();
                tmpPosition = m_tic.vars.current_position;
                tmpFloatPosition = 0;
                tmpFloatPosition += tmpPosition;
                tmpFloatPosition /= (float)nudStpsPermm.Value;
                tmpFloatPosition = Math.Abs(tmpFloatPosition);
                labelPosition.Text = (tmpFloatPosition).ToString("#0.0");
                if (lastAxis == 0)
                {
                    txtXVal.Text = (tmpFloatPosition).ToString("#0.0");
                }
                else
                {
                    txtYVal.Text = (tmpFloatPosition).ToString("#0.0");
                }
                string status = "";

                foreach (var prop in m_tic.status_vars.GetType().GetProperties())
                {
                    status = status + string.Format("{0}={1}\n\r", prop.Name, prop.GetValue(m_tic.status_vars, null));
                }

                string vars = "";


                foreach (var prop in m_tic.vars.GetType().GetProperties())
                {
                    vars = vars + string.Format("{0}={1}\n\r", prop.Name, prop.GetValue(m_tic.vars, null));
                }

                labelStatus.Text = status;
                if (m_tic.in_position())
                {
                    buttonHome.Text = "HOME";

                }
                // labelVars.Text = vars;
                if (m_moving && m_tic.in_position())
                {
                    m_moving = false;
                    buttonGoto.Text = "NEXT";

                    buttonHome.Enabled = !m_moving;
                    checkBoxJogLeft.Enabled = !m_moving;
                    checkBoxJogRigth.Enabled = !m_moving;
                }
                if (m_homing && m_tic.in_home())
                {
                    m_homing = false;
                    buttonHome.Text = "HOME";

                    //buttonGoto.Enabled = !m_homing;
                    checkBoxJogLeft.Enabled = !m_homing;
                    checkBoxJogRigth.Enabled = !m_homing;
                }

                if (m_tic.in_home())
                {
                    nextPosition = 0.0f;
                    txtSetPos.Text = "0.0";
                    buttonGoto.Text = "RUN";
                    buttonGoto.Enabled = true;
                }

                if (m_decelerating && m_tic.vars.current_velocity == 0)
                {
                    m_decelerating = false;
                    buttonHome.Enabled = true;
                }
                float tmpVoltage;
                tmpVoltage = (float)m_tic.vars.vin_voltage;
                tmpVoltage /= 1000;
                SystemVoltage.Text = "System Voltage:  " + tmpVoltage.ToString("#0.00 v"); ;
            }
            else
            {
                if (m_jogging == true)
                {

                }
            }


        }
        bool going_home = false;



        private void buttonHome_Click(object sender, EventArgs e)
        {
            float homePos = -0.0f;
            txtXVal.Text = homePos.ToString("0.0");
            runToPosition(0, homePos, true);
           

            txtYVal.Text = homePos.ToString("0.0");
            runToPosition(1, homePos, true);
            
            nextPos[0] = 0;
            nextPos[1] = 0;
        }


        private void buttonGoto_Click(object sender, EventArgs e)
        {
            float target = 0;
            int I = 0;
            buttonGoto.Enabled = false;
          //  nextPosition = TicDotNetExample.Properties.Settings.Default.startSpace;
          //  nextPosition = nextPos;
            txtSetPos.Text = nextPosition.ToString("#0.0");
            if (m_moving == false)
            {
                sampleCNT = 0;
                //if (nextPosition < (float)TicDotNetExample.Properties.Settings.Default.endSpace)
               // {
                    m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                    target = (float)nextPosition * (float)TicDotNetExample.Properties.Settings.Default.stepsPermm;
                    if (TicDotNetExample.Properties.Settings.Default.ReverseX == true)
                    {
                        m_tic.set_target_position(-(int)target);
                    }
                    else
                    {
                        m_tic.set_target_position((int)target);

                    }

                    m_moving = true;
                    Application.DoEvents();
                   //while (float.Parse(labelPosition.Text) != float.Parse(txtSetPos.Text))
                  //  {
                //      Application.DoEvents();

                   // }

                    sample2[sampleCNT, 0] = nextPosition;

                  //  nextPosition += TicDotNetExample.Properties.Settings.Default.testStep;
                    txtSetPos.Text = nextPosition.ToString("#0.0");
               
                    /* if (nextPosition > 0.5f)
                     {
                         do
                         {
                             ShowInputDialog(ref input, nextPosition - 0.5f);
                         } while (!float.TryParse(input, out inputData) && exitMeasurements == false);
                     }  */

                   // if (exitMeasurements == true)
                   // {

                   //     break;
                  //  }
                    long UtcTime;
                    UtcTime = DateTime.UtcNow.Ticks;

                   // while (UtcTime + 5000000 > (DateTime.UtcNow.Ticks)) ;
                    Delay(10);
                    if (nextPosition > 0.1f)
                    {
                        m_tic.get_variables();
                        float tmpVoltage;
                        tmpVoltage = (float)m_tic.vars.vin_voltage;
                        tmpVoltage /= 1000;

                        sample2[sampleCNT, 1] = tmpVoltage;
                        sampleCNT++;
                    }


                
               // }
                //if (exitMeasurements == false)
                //{
                //    saveResults();

               // }
               // else
               // {
               //     exitMeasurements = false;
               // }

                buttonGoto.Text = "RUN";
                nextPosition = 0.0f;
                txtSetPos.Text = nextPosition.ToString("#0.0");
                m_tic.set_target_position(-(int)0);
                going_home = true;
                m_moving = false;
            }
            else
            {


                buttonGoto.Text = "NEXT";
                m_moving = false;

            }
            m_tic.halt_and_set_position(0);
            buttonHome.Enabled = !m_moving;
            checkBoxJogLeft.Enabled = !m_moving;
            checkBoxJogRigth.Enabled = !m_moving;
            m_tic.deenergize();
            m_tic.close();
            bn_Conect.Text = "Connect";
            toolStripStatusConnection.Text = "Connection: OFF";
            m_conected = false;

        }

        void runMotor(int Axis = 0, float X = (float)0.0, bool Reset = false)
        {
            float target = 0;
            int I = 0;
            buttonGoto.Enabled = false;
            //  nextPosition = TicDotNetExample.Properties.Settings.Default.startSpace;
            nextPosition = X;
            txtSetPos.Text = nextPosition.ToString("#0.0");
            if (m_moving == false)
            {
                sampleCNT = 0;
                //if (nextPosition < (float)TicDotNetExample.Properties.Settings.Default.endSpace)
                // {
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                target = (float)X * (float)TicDotNetExample.Properties.Settings.Default.stepsPermm;
                
                if (Axis == 0)
                {
                    if (TicDotNetExample.Properties.Settings.Default.ReverseX == true)
                    {
                        m_tic.set_target_position(-(int)target);
                    }
                    else
                    {
                        m_tic.set_target_position((int)target);

                    }
                }
                else 
                {
                    if (TicDotNetExample.Properties.Settings.Default.ReverseY == true)
                    {
                        m_tic.set_target_position(-(int)target);
                    }
                    else
                    {
                        m_tic.set_target_position((int)target);

                    }
                }

                Delay(20);
                m_moving = true;
                Application.DoEvents();
                while (m_tic.vars.target_position != m_tic.vars.current_position)
                  {
                      Application.DoEvents();

                 }
                Delay(2);
                sample2[sampleCNT, 0] = nextPosition;

                //  nextPosition += TicDotNetExample.Properties.Settings.Default.testStep;
                txtSetPos.Text = nextPosition.ToString("#0.0");
              
                    m_moving = false;
                
                
                //  string input = "0.0";
                //  float inputData = 0.0f;
                /* if (nextPosition > 0.5f)
                 {
                     do
                     {
                         ShowInputDialog(ref input, nextPosition - 0.5f);
                     } while (!float.TryParse(input, out inputData) && exitMeasurements == false);
                 }  */

                // if (exitMeasurements == true)
                // {

                //     break;
                //  }
                long UtcTime;
                UtcTime = DateTime.UtcNow.Ticks;

                Delay(2);


                m_tic.get_variables();

                resetPin = (float)m_tic.vars.analog_readinng_sda;
                resetPin /= 1000;

                
                sensorValue[Axis] = ConvertToDistance(m_tic.vars.analog_readinng_scl);
               



                // }
                //if (exitMeasurements == false)
                //{
                //    saveResults();

                // }
                // else
                // {
                //     exitMeasurements = false;
                // }

                buttonGoto.Text = "RUN";
                nextPosition = 0.0f;
                txtSetPos.Text = nextPosition.ToString("#0.0");
                if (Reset)
                {
                    m_tic.set_target_position(-(int)0);
                }
              //  
              //  going_home = true;
                m_moving = false;
            }
            else
            {


                buttonGoto.Text = "NEXT";
                m_moving = false;

            }
            if (Reset)
            {
                m_tic.halt_and_set_position(0);
            }
               
                buttonHome.Enabled = !m_moving;
                checkBoxJogLeft.Enabled = !m_moving;
                checkBoxJogRigth.Enabled = !m_moving;
                m_tic.deenergize();
                m_tic.close();
                
                m_conected = false;
            
           
        }
        private float ConvertToDistance(uint sensorValueConvert)
        {
            float calValue;
            calValue = (sensorValueConvert / 65535.0f) * 50.0f;
            calValue = sensorConvert[(int)calValue];
            return calValue;
        }
        private void checkBoxJogLeft_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxJogLeft.CheckState == CheckState.Checked)
            {
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                m_tic.set_target_position((int)numericUpDownPosition.Minimum);

                buttonGoto.Enabled = false;
                buttonHome.Enabled = false;
            }
            else
            {
                m_tic.set_target_velocity(0);
                m_decelerating = true;
            }

        }

        private void checkBoxJogRigth_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxJogRigth.CheckState == CheckState.Checked)
            {
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                m_tic.set_target_position((int)numericUpDownPosition.Maximum);
                buttonGoto.Enabled = false;
                buttonHome.Enabled = false;
            }
            else
            {
                m_tic.set_target_velocity(0);
                m_decelerating = true;
            }

        }

        private void buttonEnergize_Click(object sender, EventArgs e)
        {
            if (m_tic.status_vars.energized)
            {
                m_tic.deenergize();
                buttonEnergize.Text = "ENERGIZE";
            }
            else
            {
                m_tic.clear_driver_error();
                m_tic.energize();
                m_tic.exit_safe_start();
                buttonEnergize.Text = "DEENERGIZE";
            }
        }

        private void RESET_POS_Click(object sender, EventArgs e)
        {
            m_tic.halt_and_set_position(0);
            nextPosition = 0.0f;
            sampleCNT = 0;
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            m_tic.set_target_position((int)e.NewValue);
            m_moving = true;
            buttonGoto.Text = "STOP";

            buttonHome.Enabled = !m_moving;
            checkBoxJogLeft.Enabled = !m_moving;
            checkBoxJogRigth.Enabled = !m_moving;
        }

        private void numericUpDownPosition_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.stepsPermm = (int)nudStpsPermm.Value;
            TicDotNetExample.Properties.Settings.Default.Save();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            newSpeedValue = TicDotNetExample.Properties.Settings.Default.Speed ;
            stepPerMM = TicDotNetExample.Properties.Settings.Default.stepsPermm ;
            stepDirectionX = TicDotNetExample.Properties.Settings.Default.ReverseX;
            stepDirectionY = TicDotNetExample.Properties.Settings.Default.ReverseY;
            serialXStr = TicDotNetExample.Properties.Settings.Default.serialXString ;
            serialYStr = TicDotNetExample.Properties.Settings.Default.serialYString ;

           


        }

        private void numericUpDownSpeed_ValueChanged(object sender, EventArgs e)
        {


            TicDotNetExample.Properties.Settings.Default.Speed = (int)numericUpDownSpeed.Value;
            TicDotNetExample.Properties.Settings.Default.Save();

        }

        private void numericUpDownJogSpeed_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void labelConection_Click(object sender, EventArgs e)
        {

        }

        private void chkReverse_CheckedChanged(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.ReverseX = chkReverse.Checked;
            TicDotNetExample.Properties.Settings.Default.Save();
            if (chkReverse.Checked == true)
            {
                chkReverse.Text = "REVERSE";
            }
            else
            {
                chkReverse.Text = "NORMAL";
            }
        }

        private void labelPosition_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void buttonHover(object sender, EventArgs e)
        {
            cmdJog.FlatStyle = FlatStyle.Flat;
        }

        private void cmdJog_MouseHover(object sender, EventArgs e)
        {
            cmdJog.FlatStyle = FlatStyle.Flat;
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            m_tic.set_target_position((int)-500);
        }

        private void cmdJog_MouseLeave(object sender, EventArgs e)
        {
            m_tic.set_max_speed((int)numericUpDownSpeed.Value);
            m_tic.set_target_position((int)m_tic.vars.current_position);
        }

        private void cmdJog_Click(object sender, EventArgs e)
        {

        }

        private void cmdJogUp_MouseHover(object sender, EventArgs e)
        {
            cmdJogUp.FlatStyle = FlatStyle.Flat;
            float target;
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            target = (float)6.5 * (float)nudStpsPermm.Value;
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            if (chkReverse.Checked == true)
            {
                m_tic.set_target_position(-(int)target);
            }
            else
            {
                m_tic.set_target_position((int)target);

            }
        }

        private void cmdJogUp_MouseLeave(object sender, EventArgs e)
        {

            m_tic.set_target_position((int)m_tic.vars.current_position);
        }

        private void cmdJogUp_Click(object sender, EventArgs e)
        {

        }

        private void stepsmmToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = "10000";
            string Label = "SPEED";
            ShowInputDialog(ref input, ref Label);

        }

        private static DialogResult ShowInputDialog(ref string input, ref string labelStr)
        {
            Form currentForm = Form1.ActiveForm;
            System.Drawing.Size size = new System.Drawing.Size(200, 350);


           
            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Location = new System.Drawing.Point(200, 200);
            inputBox.Left = 500;
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            inputBox.StartPosition = FormStartPosition.CenterParent;



            System.Windows.Forms.NumericUpDown updSpeedControl = new NumericUpDown();
            updSpeedControl.Size = new System.Drawing.Size(100, 23);
            updSpeedControl.Location = new System.Drawing.Point(70, 10);
            updSpeedControl.Minimum = 1000000;
            updSpeedControl.Maximum = 2000000;
            updSpeedControl.ValueChanged += new EventHandler(speedChangedEvt);
            updSpeedControl.Increment = 100000;
            int testInt = TicDotNetExample.Properties.Settings.Default.Speed;
            if ((testInt > updSpeedControl.Minimum) && (testInt < updSpeedControl.Maximum))
            {
                updSpeedControl.Value = testInt;
                newSpeedValue = testInt;

            }
            else
            {
                updSpeedControl.Value = (int)1000000;
                newSpeedValue = (int)1000000;
            }
            inputBox.Controls.Add(updSpeedControl);

            System.Windows.Forms.NumericUpDown updStepsControl = new NumericUpDown();
            updStepsControl.Size = new System.Drawing.Size(100, 23);
            updStepsControl.Location = new System.Drawing.Point(70, 40);
            updStepsControl.Minimum = 200;
            updStepsControl.Maximum = 600;
            updStepsControl.ValueChanged += new EventHandler(saveSetEvt);
            stepPerMM = TicDotNetExample.Properties.Settings.Default.stepsPermm; ;
            updStepsControl.Increment = 1;
            testInt = TicDotNetExample.Properties.Settings.Default.stepsPermm;
            if ((testInt > updStepsControl.Minimum) && (testInt < updStepsControl.Maximum))
            {
                stepPerMM = testInt;
                updStepsControl.Value = testInt;
            }
            else
            {
                stepPerMM = 315;
                updStepsControl.Value = 315;
            }

            inputBox.Controls.Add(updStepsControl);

            System.Windows.Forms.Button directionButtonX = new Button();
            directionButtonX.Size = new System.Drawing.Size(100, 23);
            directionButtonX.Location = new System.Drawing.Point(75, 70);
            if (TicDotNetExample.Properties.Settings.Default.ReverseX == true)
            {
                directionButtonX.Text = "REVERSE";
                stepDirectionX = true;
            }
            else
            {
                directionButtonX.Text = "NORMAL";
                stepDirectionX = false;
            }
            directionButtonX.TextAlign = ContentAlignment.MiddleCenter;
            directionButtonX.Click += new EventHandler(changeDirection);
            directionButtonX.Name = "X";
            inputBox.Controls.Add(directionButtonX);

            System.Windows.Forms.Button directionButtonY = new Button();
            directionButtonY.Size = new System.Drawing.Size(100, 23);
            directionButtonY.Location = new System.Drawing.Point(75, 100);
            if (TicDotNetExample.Properties.Settings.Default.ReverseY == true)
            {
                directionButtonY.Text = "REVERSE";
                stepDirectionY = true;
            }
            else
            {
                directionButtonY.Text = "NORMAL";
                stepDirectionY = false;
            }
            directionButtonY.TextAlign = ContentAlignment.MiddleCenter;
            directionButtonY.Click += new EventHandler(changeDirection);
            directionButtonY.Name = "Y";
            inputBox.Controls.Add(directionButtonY);


            /*  System.Windows.Forms.TextBox textBox = new TextBox();
              textBox.Size = new System.Drawing.Size(100,23);
              textBox.Location = new System.Drawing.Point(50, 25);
              textBox.Text = input;
              inputBox.Controls.Add(textBox);
            */
            System.Windows.Forms.Label speedLabel = new Label();
            speedLabel.Text = "SPEED";
            speedLabel.Location = new System.Drawing.Point(5, 12);
            speedLabel.AutoSize = true;
            speedLabel.Visible = true;
            inputBox.Controls.Add(speedLabel);

            System.Windows.Forms.Label StepsLabel = new Label();
            StepsLabel.Text = "STEPS";
            StepsLabel.Location = new System.Drawing.Point(5, 42);
            StepsLabel.AutoSize = true;
            StepsLabel.Visible = true;
            inputBox.Controls.Add(StepsLabel);

            System.Windows.Forms.Label dirLabelX = new Label();
            dirLabelX.Text = "POLARITY X";
            dirLabelX.Location = new System.Drawing.Point(5, 75);
            dirLabelX.AutoSize = true;
            dirLabelX.Visible = true;
            inputBox.Controls.Add(dirLabelX);

            System.Windows.Forms.Label dirLabelY = new Label();
            dirLabelY.Text = "POLARITY Y";
            dirLabelY.Location = new System.Drawing.Point(5, 105);
            dirLabelY.AutoSize = true;
            dirLabelY.Visible = true;
            inputBox.Controls.Add(dirLabelY);

            System.Windows.Forms.Button SetSerialX = new Button();
            SetSerialX.Text = "SET SERIAL X";
            SetSerialX.Location = new System.Drawing.Point(5, 140);
            SetSerialX.AutoSize = true;
            SetSerialX.Visible = true;
            SetSerialX.Click += new EventHandler(setSerialXEvt);
            inputBox.Controls.Add(SetSerialX);

            System.Windows.Forms.Button SetSerialY = new Button();
            SetSerialY.Text = "SET SERIAL Y";
            SetSerialY.Location = new System.Drawing.Point(5, 170);
            SetSerialY.AutoSize = true;
            SetSerialY.Visible = true;
            SetSerialY.Click += new EventHandler(setSerialYEvt);
            inputBox.Controls.Add(SetSerialY);

            System.Windows.Forms.Button cmdXSetting = new Button();
            cmdXSetting.Text = "SET SETTINGS";
            cmdXSetting.Location = new System.Drawing.Point(50, 200);
            cmdXSetting.AutoSize = true;
            cmdXSetting.Visible = true;
            cmdXSetting.Click += new EventHandler(setXSettingsEvt);
            inputBox.Controls.Add(cmdXSetting);

            

            lblSerialX.Text = serialXStr;
            lblSerialX.Location = new System.Drawing.Point(95, 145);
            lblSerialX.AutoSize = false;
            lblSerialX.BorderStyle = BorderStyle.FixedSingle;
            lblSerialX.Width = 100;
            lblSerialX.Height = 15;
            lblSerialX.Visible = true;
            inputBox.Controls.Add(lblSerialX);

            
            lblSerialY.Text = serialYStr;
            lblSerialY.Location = new System.Drawing.Point(95, 175);
            lblSerialY.AutoSize = false;
            lblSerialY.BorderStyle = BorderStyle.FixedSingle;
            lblSerialY.Width = 100;
            lblSerialY.Height = 15;
            lblSerialY.Visible = true;
            inputBox.Controls.Add(lblSerialY);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&SAVE";
            okButton.Location = new System.Drawing.Point(10, 300);
            okButton.Click += new EventHandler(saveSettingEvt);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 300);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();



            return result;
        }

        private static void setYSettingsEvt(object sender, EventArgs e)
        {
            string ticCmd = "ticcmd -d 00356063 --set-settings test.txt";
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();

            FileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = "c:\\Temp\\SetY.Bat";
           
            string fileName = saveFileDialog1.FileName;

            TextWriter txw = new StreamWriter(fileName);
            ticCmd = string.Format("ticcmd -d {0} --set-settings test.txt", serialYStr);
            txw.WriteLine(ticCmd);
            txw.Close();
        }

        private static void setXSettingsEvt(object sender, EventArgs e)
        {
        
            saveXBat();
            saveYBat();
        }


        static void saveXBat()
        {
            string ticCmd = "ticcmd -d 00356063 --set-settings setting.txt";
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            FileDialog saveFileDialog1 = new SaveFileDialog();
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string directory = System.IO.Path.GetDirectoryName(exePath);
            saveFileDialog1.FileName = directory + "\\SetX.Bat";

            string fileName = saveFileDialog1.FileName;

            TextWriter txw = new StreamWriter(fileName);
            ticCmd = string.Format("ticcmd -d {0} --set-settings test.txt", serialXStr);
            txw.WriteLine(ticCmd);
            txw.Close();
            ExecuteCommand(saveFileDialog1.FileName);
        }

        static void saveYBat()
        {
            string ticCmd = "ticcmd -d 00356063 --set-settings test.txt";
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            FileDialog saveFileDialog1 = new SaveFileDialog();
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string directory = System.IO.Path.GetDirectoryName(exePath);
            saveFileDialog1.FileName = directory + "\\SetY.Bat";

            string fileName = saveFileDialog1.FileName;

            TextWriter txw = new StreamWriter(fileName);
            ticCmd = string.Format("ticcmd -d {0} --set-settings test.txt", serialYStr);
            txw.WriteLine(ticCmd);
            txw.Close();
            ExecuteCommand(saveFileDialog1.FileName);
        }

        static void ExecuteCommand(string command)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);

            process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
                Console.WriteLine("output>>" + e.Data);
            process.BeginOutputReadLine();

            process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
                Console.WriteLine("error>>" + e.Data);
            process.BeginErrorReadLine();

            process.WaitForExit();

            Console.WriteLine("ExitCode: {0}", process.ExitCode);
            process.Close();
        }
        static void setSettingsEvt(object sender, EventArgs e)
        {
            string ticCmd = "ticcmd -d 00356063 --set-settings test.txt";
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            FileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = "SetY.Bat";
            openFileDialog.ShowDialog();
            DialogResult  result = saveFileDialog1.ShowDialog();

            string fileName = saveFileDialog1.FileName;

            TextWriter txw = new StreamWriter(fileName);
            ticCmd = string.Format("ticcmd - d {0}--set - settings test.txt", serialXStr);
            txw.WriteLine(ticCmd);
            txw.Close();

            saveFileDialog1.FileName = "SetY.Bat";
            fileName = saveFileDialog1.FileName;
            openFileDialog.ShowDialog();
            DialogResult resultY = saveFileDialog1.ShowDialog();
            TextWriter tyw = new StreamWriter(fileName);
            ticCmd = string.Format("ticcmd - d {0}--set - settings test.txt", serialYStr);
            tyw.WriteLine(ticCmd);
            tyw.Close();
        }

        private static void setSerialXEvt(object sender, EventArgs e)
        {
            MessageBox.Show("Make Sure onlY X USB is connected");

            serialXStr = m_tic.getUSBSerial(tic.PRODUCT_ID.T36V4, "");
            lblSerialX.Text = serialXStr;
        }

        private static void  setSerialYEvt(object sender, EventArgs e)
        {
          
            MessageBox.Show("Make Sure onlY Y USB is connected");
           
            serialYStr = m_tic.getUSBSerial(tic.PRODUCT_ID.T36V4, "");
            lblSerialY.Text = serialYStr;
           
        
        }

        private static void chgSerialYEvt(object sender, EventArgs e)
        {
            TextBox serialTextBox = (TextBox)sender;
            serialYStr = serialTextBox.Text;
        }

        private static void chgSerialXEvt(object sender, EventArgs e)
        {
            TextBox serialTextBox = (TextBox)sender;
            serialXStr = serialTextBox.Text;
        }

        private static void saveStepEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            stepTest = (float)newUpDown.Value;
        }

        private static void saveEndEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            endTest = (float)newUpDown.Value;
        }

        private static void saveStartEvt(object sender, EventArgs e)
        {

            NumericUpDown newUpDown = (NumericUpDown)sender;
            startTest = (float)newUpDown.Value;

        }

        private static void changeDirection(object sender, EventArgs e)
        {

            Button newButton = (Button)sender;
            if (newButton.Name == "X")
            {
                if (stepDirectionX == true)
                {
                    stepDirectionX = false;
                    newButton.Text = "NORMAL";

                }
                else
                {
                    stepDirectionX = true;
                    newButton.Text = "REVERSE";
                }
            }
            else
            {
                if (stepDirectionY == true)
                {
                    stepDirectionY = false;
                    newButton.Text = "NORMAL";
                }
                else
                {
                    stepDirectionY = true;
                    newButton.Text = "REVERSE";
                }
            }
        }

        private static void saveSettingEvt(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.Speed = newSpeedValue;
            TicDotNetExample.Properties.Settings.Default.stepsPermm = stepPerMM;
            TicDotNetExample.Properties.Settings.Default.ReverseX = stepDirectionX;
            TicDotNetExample.Properties.Settings.Default.ReverseY = stepDirectionY;
            TicDotNetExample.Properties.Settings.Default.startSpace = startTest;
            TicDotNetExample.Properties.Settings.Default.endSpace = endTest;
            TicDotNetExample.Properties.Settings.Default.testStep = stepTest;
            TicDotNetExample.Properties.Settings.Default.serialXString = serialXStr;
            TicDotNetExample.Properties.Settings.Default.serialYString = serialYStr;
            TicDotNetExample.Properties.Settings.Default.Save();
        }

        private static void saveSetEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            stepPerMM = (int)newUpDown.Value;
        }

        private static void speedChangedEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            newSpeedValue = (int)newUpDown.Value;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int I;



            File.WriteAllText("c:/test/WriteLines.txt", "Spacing, Reading\r\n");
            for (I = 0; I < sampleCNT; I++)
            {
                File.AppendAllText("c:/test/WriteLines.txt", sample2[I, 0].ToString("#0.00"));
                File.AppendAllText("c:/test/WriteLines.txt", ", ");
                File.AppendAllText("c:/test/WriteLines.txt", sample2[I, 1].ToString("#0.00\r\n"));
            }


        }
        void saveResults()
        {

            int I;
            var filePath = string.Empty;

            var fileName = string.Empty;
            fileName = "\\Test Date_" + DateTime.UtcNow.ToLongDateString();
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            openFileDialog.SelectedPath = TicDotNetExample.Properties.Settings.Default.SettingFilePath;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                filePath = openFileDialog.SelectedPath + fileName;
                TicDotNetExample.Properties.Settings.Default.SettingFilePath = openFileDialog.SelectedPath;
                TicDotNetExample.Properties.Settings.Default.Save();

            }


            File.WriteAllText(filePath, "Spacing, Reading\r\n");
            for (I = 0; I < sampleCNT; I++)
            {
                File.AppendAllText(filePath, sample2[I, 0].ToString("#0.00"));
                File.AppendAllText(filePath, ", ");
                File.AppendAllText(filePath, sample2[I, 1].ToString("#0.00\r\n"));
            }


        }
        private static DialogResult ShowInputDialog(ref string input, float setPoint)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Enter Measurement";
            inputBox.StartPosition = FormStartPosition.CenterParent;
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(50, 100);

            textBox.Location = new System.Drawing.Point(75, 5);
            textBox.Text = "";
            textBox.KeyPress += new KeyPressEventHandler(checkForNumber);
            inputBox.Controls.Add(textBox);

            Label newLabel = new Label();
            newLabel.Text = setPoint.ToString("#0.00 mm");
            newLabel.Location = new System.Drawing.Point(15, 7);
            inputBox.Controls.Add(newLabel);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Click += new EventHandler(quitRun);
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private static void quitRun(object sender, EventArgs e)
        {
            exitMeasurements = true;
        }

        private static void checkForNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.')//The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }
        private static DialogResult ShowSplash()
        {
            Form currentForm = Form1.ActiveForm;
            System.Drawing.Size size = new System.Drawing.Size(400, 200);


            Form splashScrn = new Form();
            splashScrn.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            splashScrn.ClientSize = size;
            splashScrn.BackColor = Color.LightGray;
            splashScrn.Location = new System.Drawing.Point(200, 100);
            splashScrn.Left = 500;
            splashScrn.MaximizeBox = false;
            splashScrn.MinimizeBox = false;
            splashScrn.ControlBox = false;
            splashScrn.StartPosition = FormStartPosition.CenterParent;

            PictureBox logoPic = new PictureBox();

            logoPic.Size = new System.Drawing.Size(400, 100);
            logoPic.Location = new System.Drawing.Point(40, 10);
           
            Bitmap original = (Bitmap)Image.FromFile("Logo.bmp");
            Bitmap resized = new Bitmap(original, new Size(original.Width / 2, original.Height / 2));
            logoPic.Image = resized;
            splashScrn.Controls.Add(logoPic);

            Label AssemblyLabel = new Label();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
            AssemblyLabel.Text = String.Format("Title: {0}", titleAttribute.Title);
            AssemblyLabel.Location = new System.Drawing.Point(15, 110);

            AssemblyLabel.Size = new System.Drawing.Size(150, 15);
            splashScrn.Controls.Add(AssemblyLabel);

            Label VesionLabel = new Label();
            VesionLabel.Text = String.Format("Version: {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());
            VesionLabel.Location = new System.Drawing.Point(15, 130);

            VesionLabel.Size = new System.Drawing.Size(150, 15);
            splashScrn.Controls.Add(VesionLabel);

            Label copyLabel = new Label();
            object[] attributes2 = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);


            copyLabel.Text = ((AssemblyCopyrightAttribute)attributes2[0]).Copyright;
            copyLabel.Location = new System.Drawing.Point(15, 150);

            copyLabel.Size = new System.Drawing.Size(150, 15);
            splashScrn.Controls.Add(copyLabel);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(120, 170);
            splashScrn.Controls.Add(okButton);

            DialogResult result = splashScrn.ShowDialog();



            return result;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSplash();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        static void Delay(int timeWait)
        {
            timerCNT = timeWait;
            while (timerCNT > 0)
            {
                Application.DoEvents();
            }
        }
        private void txtSetPos_TextChanged(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        static int[] nextPos = { 0, 0 };
       

        private void cmdRunX_Click(object sender, EventArgs e)
        {
            
            txtXVal.Text = newCal[nextPos[0], 0].ToString("0.0");
            runToPosition(0, newCal[nextPos[0], 0], false);
            nextPos[0]++;
        }

       
        
        private void cmdRunY_Click(object sender, EventArgs e)
        {
            txtYVal.Text = newCal[nextPos[1], 1].ToString("0.0");
            runToPosition(1, newCal[nextPos[1], 1],  false);
            nextPos[1]++  ;
        }
        
        private void cmdResetX_Click(object sender, EventArgs e)
        {
            int maxDist = 0;
            float ZeroDist = 0.0f;
            cmdResetX.Enabled = false; 
            resetPin = 10.0f;
            lastAxis =  0;
            runToPosition(0, 2.5f, true);
            while ((resetPin > 5.0) && (maxDist++ < 20))
            {
                m_conected = false;
                ZeroDist += 0.5f;
                runToPosition(0, -ZeroDist, false);
                
            }
            runToPosition(0, -ZeroDist, true);
            nextPos[0] = 0;
            if (maxDist >= 21)
            {
                MessageBox.Show("FAILED TO ZERO");
            }
            cmdResetX.Enabled = true;


        }

        private void cmdResetY_Click(object sender, EventArgs e)
        {
            int maxDist = 0;
            cmdResetY.Enabled = false;
            resetPin = 10.0f;
            lastAxis = 1;
            runToPosition(1, 5, true);
            while ((resetPin > 5.0) && (maxDist++ < 20))
            {
                m_conected = false;
                runToPosition(1, -0.5f, true);

            }
            nextPos[1] = 0;
            if (maxDist == 21)
            {
                MessageBox.Show("FAILED TO ZERO");
            }
            cmdResetY.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string input = "10000";
            string Label = "Speed";
            ShowInputDialog(ref input, ref Label);
        }
        static float oldX = 9.999f;
        static float oldY = 9.999f;
        private void cmdRUNAll_Click(object sender, EventArgs e)
        {
            float[,] testResults = new float[30, 2];
            int testNumber = 0 ;
            cmdRUNAll.Enabled = false;
            nextPos[0] = 0;
            nextPos[1] = 0;
            while (newCal[nextPos[0], 0] != 0.0f)
            {
                if (oldX != newCal[nextPos[0], 0])
                {
                    lastAxis = 0;
                    runToPosition(0, newCal[nextPos[0], 0], false);
                    oldX = newCal[nextPos[0], 0];
                    txtXVal.Text = newCal[nextPos[0], 0].ToString("0.0");
                }
                nextPos[0]++;

                if (oldY != newCal[nextPos[1], 1])
                {
                    lastAxis = 1;
                    runToPosition(1, newCal[nextPos[1], 1], false);
                    oldY = newCal[nextPos[1], 1];
                    txtYVal.Text = newCal[nextPos[1], 1].ToString("0.0");
                }
                nextPos[1]++;
                txtValX.Text = sensorValue[0].ToString("0.000");
                txtValY.Text = sensorValue[1].ToString("0.000");
                testResults[testNumber, 0] = sensorValue[0];
                testResults[testNumber, 1] = sensorValue[1];
                testNumber++;
                Delay(20);
                
            }
            lastAxis = 0;
            runToPosition(0, 0.0f, false);
            lastAxis = 1;
            runToPosition(1, 0.0f, false);
            DialogResult result = this.folderBrowserDialog1.ShowDialog();
            this.saveFileDialog1.FileName = String.Format("Sensor Test {0}.csv",
                                DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss"));

            result = this.saveFileDialog1.ShowDialog();
            string fileName = this.saveFileDialog1.FileName;

            TextWriter tw = new StreamWriter(fileName);
           // FileStream fs = File.Create(fileName);
          
            string output = "{1}, {1}, {1},{1}";
            for (int I = 0;I < 25;I++)
            {
                
                output = string.Format("{0}, {1}, {2},{3}", newCal[I, 0], newCal[I, 1], testResults[I, 0], testResults[I, 1]);
                tw.WriteLine(output);
            }


            tw.Close();
            cmdRUNAll.Enabled = true;
        }
        
    }

    
}

       
    



