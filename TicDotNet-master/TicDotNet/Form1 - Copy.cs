﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using Microsoft.VisualBasic;
using System.Timers;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        tic m_tic;

        bool m_conected = false;
        bool m_moving=false;
        bool m_homing = false;
        bool m_jogging = false;
        static bool exitMeasurements = false;
        static int newSpeedValue = 0;
        static int stepPerMM = 0;
        static bool stepDirection;
        private bool m_decelerating;
        int sampleCNT = 0;
        
        float[,] sample2 = new float[15, 2];
        

        float nextPosition = 0.0f;
       
        public Form1()
        {
            InitializeComponent();
          //  ShowSplash();
            buttonGoto.Enabled = m_conected;
            buttonHome.Enabled = m_conected;
            checkBoxJogLeft.Enabled = m_conected;
            checkBoxJogRigth.Enabled = m_conected;
            buttonEnergize.Enabled = m_conected;
            m_tic = new tic();
        }

        private void bn_Conect_Click(object sender, EventArgs e)
        {

            try
            {
                if (!m_conected)
                {
                    m_conected = m_tic.open(tic.PRODUCT_ID.T36V4);
                    m_tic.reinitialize();
//                    m_tic.reset();
                    m_tic.energize();
                    m_tic.clear_driver_error();

                    //m_tic.set_max_accel(100000);
                    //m_tic.set_max_decel(100000);
                    //m_tic.set_max_speed(50000000);
                    //m_tic.set_starting_speed(2000000);
                    m_tic.exit_safe_start();
                    m_tic.wait_for_device_ready();
                    bn_Conect.Text = "Disconnect";
                    toolStripStatusConnection.Text = "Connection: ACTIVE";
                    
                }
                else
                {
                    m_tic.deenergize();
                    m_tic.close();
                    bn_Conect.Text="Connect";
                    toolStripStatusConnection.Text ="Connection: OFF";
                    m_conected = false;
                }

                buttonGoto.Enabled = m_conected;
                buttonHome.Enabled = m_conected;
                checkBoxJogLeft.Enabled = m_conected;
                checkBoxJogRigth.Enabled = m_conected;
                buttonEnergize.Enabled = m_conected;
                RESET_POS.Enabled = m_conected;
                cmdJog.Enabled = m_conected;
                cmdJogUp.Enabled = m_conected;
                m_tic.halt_and_set_position(0);
            }
            catch (Exception ex)
            {
                m_conected = false;
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
            Int32 tmpPosition;
            float tmpFloatPosition;
            if (m_conected)
            {
                m_tic.reset_command_timeout();
                m_tic.get_variables();
                m_tic.get_status_variables();
                tmpPosition = m_tic.vars.current_position;
                tmpFloatPosition = 0;
                tmpFloatPosition += tmpPosition;
                tmpFloatPosition /= (float)nudStpsPermm.Value;
                tmpFloatPosition = Math.Abs(tmpFloatPosition);
                labelPosition.Text = (tmpFloatPosition).ToString("#0.00");


                string status = "";

                foreach (var prop in m_tic.status_vars.GetType().GetProperties())
                {
                    status = status + string.Format("{0}={1}\n\r", prop.Name, prop.GetValue(m_tic.status_vars, null));
                }
               
                string vars = "";


                foreach (var prop in m_tic.vars.GetType().GetProperties())
                {
                    vars = vars + string.Format("{0}={1}\n\r", prop.Name, prop.GetValue(m_tic.vars, null));
                }
               
                labelStatus.Text = status;
                if (m_tic.in_position())
                {
                    buttonHome.Text = "HOME";
                   
                }
               // labelVars.Text = vars;
                if (m_moving && m_tic.in_position())
                {
                    m_moving = false;
                    buttonGoto.Text = "NEXT";
                    
                    buttonHome.Enabled = !m_moving;
                    checkBoxJogLeft.Enabled = !m_moving;
                    checkBoxJogRigth.Enabled = !m_moving;
                }
                if (m_homing && m_tic.in_home())
                {
                    m_homing = false;
                    buttonHome.Text = "HOME";
                   
                    buttonGoto.Enabled = !m_homing;
                    checkBoxJogLeft.Enabled = !m_homing;
                    checkBoxJogRigth.Enabled = !m_homing;
                }

                if (m_tic.in_home())
                {
                    nextPosition = 0.0f;
                    txtSetPos.Text = "0.00";
                    buttonGoto.Text = "START";
                   
                }

                if(m_decelerating&&m_tic.vars.current_velocity==0)
                {
                    m_decelerating = false;
                    buttonGoto.Enabled = true;
                    buttonHome.Enabled = true;
                }

            }
            else
            {
                if (m_jogging == true)
                {

                }
            }

        }
        bool going_home = false;

        

        private void buttonHome_Click(object sender, EventArgs e)
        {
            if (going_home == false)
            {
                m_tic.set_target_position(-(int)0);
                going_home = true;
                buttonHome.Text = "STOP ";

            }
            else
            {
                m_tic.set_target_position((int)m_tic.vars.current_position);
                going_home = false;
                buttonHome.Text = "HOME ";
            }
          
        }


        private void buttonGoto_Click(object sender, EventArgs e)
        {
            float target = 0;
            int I = 0;
         
            nextPosition += 0.5f;

            txtSetPos.Text = nextPosition.ToString("#0.00");
            if (m_moving == false)
            {
                sampleCNT = 0;
                for (I = 0; I < 13; I++)
                {
                    m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                    target = (float)nextPosition * (float)TicDotNetExample.Properties.Settings.Default.stepsPermm;
                    if (TicDotNetExample.Properties.Settings.Default.Reverse == true)
                    {
                        m_tic.set_target_position(-(int)target);
                    }
                    else
                    {
                        m_tic.set_target_position((int)target);

                    }

                    m_moving = true;
                    Application.DoEvents();
                    while (labelPosition.Text != txtSetPos.Text)
                    {
                        Application.DoEvents();

                    }
                    m_moving = false;
                    sample2[sampleCNT, 0] = nextPosition;
                    string input = "0.0";
                    float inputData = 0.0f;
                    if (nextPosition != 0.0f) {
                        do
                        {
                            ShowInputDialog(ref input, nextPosition);
                        } while (!float.TryParse(input, out inputData) && exitMeasurements == false);
                    }
                    nextPosition += 0.5f;
                    txtSetPos.Text = nextPosition.ToString("#0.00");

                  


                if (exitMeasurements == true)
                    {
                        sampleCNT = 0;
                        m_moving = false;
                        break;
                    }
                    sample2[sampleCNT, 1] = float.Parse(input);
                    sampleCNT++;

                }
               
                buttonGoto.Text = "RUN";
                nextPosition = 0.0f;
                txtSetPos.Text = nextPosition.ToString("#0.00");
                m_tic.set_target_position(-(int)0);
                going_home = true;
                m_moving = false;

                if (exitMeasurements == false)
                {
                    saveResults();
                }
                   
            }
            else
            {
               
               
                buttonGoto.Text = "NEXT";
                m_moving = false;

            }
            buttonHome.Enabled = !m_moving;
            checkBoxJogLeft.Enabled = !m_moving;
            checkBoxJogRigth.Enabled = !m_moving;

        }

        private void checkBoxJogLeft_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxJogLeft.CheckState==CheckState.Checked)
            {
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                m_tic.set_target_position((int)numericUpDownPosition.Minimum);

                buttonGoto.Enabled = false;
                buttonHome.Enabled = false;
            }
            else
            {
                m_tic.set_target_velocity(0);
                m_decelerating=true;
            }

        }

        private void checkBoxJogRigth_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxJogRigth.CheckState == CheckState.Checked)
            {
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                m_tic.set_target_position((int)numericUpDownPosition.Maximum);
                buttonGoto.Enabled = false;
                buttonHome.Enabled = false;
            }
            else
            {
                m_tic.set_target_velocity(0);
                m_decelerating = true;
            }

        }

        private void buttonEnergize_Click(object sender, EventArgs e)
        {
            if (m_tic.status_vars.energized)
            {
                m_tic.deenergize();
                buttonEnergize.Text="ENERGIZE";
            }
            else
            {
                m_tic.clear_driver_error();
                m_tic.energize();
                m_tic.exit_safe_start();
                buttonEnergize.Text = "DEENERGIZE";
            }
        }

        private void RESET_POS_Click(object sender, EventArgs e)
        {
            m_tic.halt_and_set_position(0);
            nextPosition = 0.0f;
            sampleCNT = 0;
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
           
                m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
                m_tic.set_target_position((int)e.NewValue);
                m_moving = true;
                buttonGoto.Text = "STOP";
            
            buttonHome.Enabled = !m_moving;
            checkBoxJogLeft.Enabled = !m_moving;
            checkBoxJogRigth.Enabled = !m_moving;
        }

        private void numericUpDownPosition_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.stepsPermm = (int)nudStpsPermm.Value;
            TicDotNetExample.Properties.Settings.Default.Save();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
            chkReverse.Checked = TicDotNetExample.Properties.Settings.Default.Reverse;
            if (chkReverse.Checked == true)
            {
                chkReverse.Text = "REVERSE";
            }
            else
            {
                chkReverse.Text = "NORMAL";
            }
        }

        private void numericUpDownSpeed_ValueChanged(object sender, EventArgs e)
        {
           

            TicDotNetExample.Properties.Settings.Default.Speed= (int)TicDotNetExample.Properties.Settings.Default.Speed;
            TicDotNetExample.Properties.Settings.Default.Save();

        }

        private void numericUpDownJogSpeed_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void labelConection_Click(object sender, EventArgs e)
        {

        }

        private void chkReverse_CheckedChanged(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.Reverse = chkReverse.Checked;
            TicDotNetExample.Properties.Settings.Default.Save();
            if (chkReverse.Checked == true)
            {
                chkReverse.Text = "REVERSE";
            }
            else
            {
                chkReverse.Text = "NORMAL";
            }
        }

        private void labelPosition_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void buttonHover(object sender, EventArgs e)
        {
            cmdJog.FlatStyle = FlatStyle.Flat;
        }

        private void cmdJog_MouseHover(object sender, EventArgs e)
        {
            cmdJog.FlatStyle = FlatStyle.Flat;
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            m_tic.set_target_position((int)-500);
        }

        private void cmdJog_MouseLeave(object sender, EventArgs e)
        {
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            m_tic.set_target_position((int)m_tic.vars.current_position);
        }

        private void cmdJog_Click(object sender, EventArgs e)
        {

        }

        private void cmdJogUp_MouseHover(object sender, EventArgs e)
        {
            cmdJogUp.FlatStyle = FlatStyle.Flat;
            float target;
            m_tic.set_max_speed((int)TicDotNetExample.Properties.Settings.Default.Speed);
            target = (float)6.5 * (float)nudStpsPermm.Value;
            m_tic.set_max_speed((int)numericUpDownSpeed.Value);
            if (chkReverse.Checked == true)
            {
                m_tic.set_target_position(-(int)target);
            }
            else
            {
                m_tic.set_target_position((int)target);

            }
        }

        private void cmdJogUp_MouseLeave(object sender, EventArgs e)
        {
            
            m_tic.set_target_position((int)m_tic.vars.current_position);
        }

        private void cmdJogUp_Click(object sender, EventArgs e)
        {

        }

        private void stepsmmToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = "10000";
            string Label = "SPEED";
            ShowInputDialog(ref input, ref Label);
            
        }
       
        private static DialogResult ShowInputDialog(ref string input, ref string labelStr)
        {
            Form currentForm = Form1.ActiveForm;
            System.Drawing.Size size = new System.Drawing.Size(200, 150);


            Form inputBox = new Form();
            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Location = new System.Drawing.Point(200, 100);
            inputBox.Left = 500;
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            inputBox.StartPosition = FormStartPosition.CenterParent;
          


            System.Windows.Forms.NumericUpDown updSpeedControl = new NumericUpDown();
            updSpeedControl.Size = new System.Drawing.Size(100, 23);
            updSpeedControl.Location = new System.Drawing.Point(70, 10);
            updSpeedControl.Minimum = 1000000;
            updSpeedControl.Maximum = 2000000;
            updSpeedControl.ValueChanged += new EventHandler(speedChangedEvt);
            updSpeedControl.Increment = 100000;
            int testInt = TicDotNetExample.Properties.Settings.Default.Speed;
            if ((testInt > updSpeedControl.Minimum) && (testInt < updSpeedControl.Maximum))
            {
                updSpeedControl.Value = testInt;
                newSpeedValue = testInt;

            }
            else
            {
                updSpeedControl.Value = (int)1000000;
                newSpeedValue = (int)1000000;
            }
            inputBox.Controls.Add(updSpeedControl);

            System.Windows.Forms.NumericUpDown updStepsControl = new NumericUpDown();
            updStepsControl.Size = new System.Drawing.Size(100, 23);
            updStepsControl.Location = new System.Drawing.Point(70, 40);
            updStepsControl.Minimum = 200;
            updStepsControl.Maximum = 600;
            updStepsControl.ValueChanged += new EventHandler(saveSetEvt);
            stepPerMM = TicDotNetExample.Properties.Settings.Default.stepsPermm; ;
            updStepsControl.Increment = 1;
            testInt = TicDotNetExample.Properties.Settings.Default.stepsPermm;
            if ((testInt > updStepsControl.Minimum) && (testInt < updStepsControl.Maximum))
            {
                stepPerMM = testInt;
                updStepsControl.Value = testInt;
            }
            else
            {
                stepPerMM = 315;
                updStepsControl.Value = 315;
            }

            inputBox.Controls.Add(updStepsControl);

            System.Windows.Forms.Button directionButton = new Button();
            directionButton.Size = new System.Drawing.Size(100, 23);
            directionButton.Location = new System.Drawing.Point(70, 70);
            if (TicDotNetExample.Properties.Settings.Default.Reverse == true)
            {
                directionButton.Text = "REVERSE";
                stepDirection = true;
            }
            else
            {
                directionButton.Text = "NORMAL";
                stepDirection = false;
            }
            directionButton.TextAlign = ContentAlignment.MiddleCenter;
            directionButton.Click += new EventHandler(changeDirection);
            inputBox.Controls.Add(directionButton);


            /*  System.Windows.Forms.TextBox textBox = new TextBox();
              textBox.Size = new System.Drawing.Size(100,23);
              textBox.Location = new System.Drawing.Point(50, 25);
              textBox.Text = input;
              inputBox.Controls.Add(textBox);
            */
            System.Windows.Forms.Label speedLabel = new Label();
            speedLabel.Text = "SPEED";
            speedLabel.Location = new System.Drawing.Point(5, 12);
            speedLabel.AutoSize = true;
            speedLabel.Visible = true;
            inputBox.Controls.Add(speedLabel);

            System.Windows.Forms.Label StepsLabel = new Label();
            StepsLabel.Text = "STEPS";
            StepsLabel.Location = new System.Drawing.Point(5, 42);
            StepsLabel.AutoSize = true;
            StepsLabel.Visible = true;
            inputBox.Controls.Add(StepsLabel);

            System.Windows.Forms.Label dirLabel = new Label();
            dirLabel.Text = "POLARITY";
            dirLabel.Location = new System.Drawing.Point(5, 72);
            dirLabel.AutoSize = true;
            dirLabel.Visible = true;
            inputBox.Controls.Add(dirLabel);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&SAVE";
            okButton.Location = new System.Drawing.Point(10, 100);
            okButton.Click += new EventHandler(saveSettingEvt);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 100);
            inputBox.Controls.Add(cancelButton);
           
            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
           

            
            return result;
        }

        private static void changeDirection(object sender, EventArgs e)
        {
            
            Button newButton = (Button)sender;
            if (stepDirection == true)
            {
                stepDirection = false;
               
            }
            else
            {
                stepDirection = true;
               
            }
            
            if (stepDirection == true)
            {
                newButton.Text = "REVERSE";
            }
            else
            {
                newButton.Text = "NORMAL";
            }
        }

        private static void saveSettingEvt(object sender, EventArgs e)
        {
            TicDotNetExample.Properties.Settings.Default.Speed = newSpeedValue;
            TicDotNetExample.Properties.Settings.Default.stepsPermm = stepPerMM;
            TicDotNetExample.Properties.Settings.Default.Reverse = stepDirection;
            TicDotNetExample.Properties.Settings.Default.Save();
        }

        private static void saveSetEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            stepPerMM = (int)newUpDown.Value;
        }

        private static void speedChangedEvt(object sender, EventArgs e)
        {
            NumericUpDown newUpDown = (NumericUpDown)sender;
            newSpeedValue = (int)newUpDown.Value;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int I;
            var filePath = string.Empty;
           

            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            openFileDialog.SelectedPath = TicDotNetExample.Properties.Settings.Default.SettingFilePath;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                filePath = openFileDialog.SelectedPath + "/test.csv";
                TicDotNetExample.Properties.Settings.Default.SettingFilePath = openFileDialog.SelectedPath;
                TicDotNetExample.Properties.Settings.Default.Save();

            }

         
            File.WriteAllText(filePath, "Spacing, Reading\r\n");
            for (I = 0; I < sampleCNT; I++)
            {
                File.AppendAllText(filePath, sample2[I, 0].ToString("#0.00"));
                File.AppendAllText(filePath, ", ");
                    File.AppendAllText(filePath, sample2[I, 1].ToString("#0.00\r\n"));
                }
         

        }
        void saveResults()
        {

            int I;
            var filePath = string.Empty;


            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            openFileDialog.SelectedPath = TicDotNetExample.Properties.Settings.Default.SettingFilePath;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Get the path of specified file
                filePath = openFileDialog.SelectedPath + "/test.csv";
                TicDotNetExample.Properties.Settings.Default.SettingFilePath = openFileDialog.SelectedPath;
                TicDotNetExample.Properties.Settings.Default.Save();

            }


            File.WriteAllText(filePath, "Spacing, Reading\r\n");
            for (I = 0; I < sampleCNT; I++)
            {
                File.AppendAllText(filePath, sample2[I, 0].ToString("#0.00"));
                File.AppendAllText(filePath, ", ");
                File.AppendAllText(filePath, sample2[I, 1].ToString("#0.00\r\n"));
            }


        }
        private static DialogResult ShowInputDialog(ref string input, float setPoint)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Enter Measurement";
            inputBox.StartPosition = FormStartPosition.CenterParent;
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(50, 100);
           
            textBox.Location = new System.Drawing.Point(75, 5);
            textBox.Text = "";
            textBox.KeyPress += new KeyPressEventHandler(checkForNumber);
            inputBox.Controls.Add(textBox);

            Label newLabel = new Label();
            newLabel.Text = setPoint.ToString("#0.00 mm");
            newLabel.Location = new System.Drawing.Point(15, 7);
            inputBox.Controls.Add(newLabel);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Click += new EventHandler(quitRun);
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private static void quitRun(object sender, EventArgs e)
        {
            exitMeasurements = true;
        }

        private static void checkForNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.')//The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }
        private static DialogResult ShowSplash()
        {
            Form currentForm = Form1.ActiveForm;
            System.Drawing.Size size = new System.Drawing.Size(400, 200);
            Form splashScrn = new Form();

            splashScrn.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            splashScrn.ClientSize = size;
            splashScrn.Location = new System.Drawing.Point(200, 100);
            splashScrn.Left = 500;
            splashScrn.MaximizeBox = false;
            splashScrn.MinimizeBox = false;
            splashScrn.StartPosition = FormStartPosition.CenterParent;

            PictureBox logoPic = new PictureBox();

            logoPic.Size = new System.Drawing.Size(400, 150);
            logoPic.Location = new System.Drawing.Point(40, 10);
            logoPic.Image = Image.FromFile("c:/test/Logo.bmp");
            splashScrn.Controls.Add(logoPic);

          /* System.Timers.Timer  _timer = new System.Timers.Timer(2000);
            _timer.Enabled = true;
            _timer.Elapsed += closeTimerEvent;
          */
                DialogResult result = splashScrn.ShowDialog();



            return result;
        }

        private static void closeTimerEvent(object sender, ElapsedEventArgs e)
        {
            System.Timers.Timer newTimer = (System.Timers.Timer)sender;
            Console.WriteLine("In TimerCallback: " + DateTime.Now);
           
        }

        private static void TimerCallback(Object o)
        {
            // Display the date/time when this method got called.
            Console.WriteLine("In TimerCallback: " + DateTime.Now);
        }

       
    }

       
    

}

