﻿namespace WindowsFormsApplication3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bn_Conect = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.buttonGoto = new System.Windows.Forms.Button();
            this.groupBoxGOTO = new System.Windows.Forms.GroupBox();
            this.RESET_POS = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPosition = new System.Windows.Forms.Label();
            this.txtSetPos = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEnergize = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.nudStpsPermm = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownSpeed = new System.Windows.Forms.NumericUpDown();
            this.chkReverse = new System.Windows.Forms.CheckBox();
            this.numericUpDownPosition = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelVars = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmdJogUp = new System.Windows.Forms.Button();
            this.cmdJog = new System.Windows.Forms.Button();
            this.checkBoxJogRigth = new System.Windows.Forms.CheckBox();
            this.checkBoxJogLeft = new System.Windows.Forms.CheckBox();
            this.cmdRunY = new System.Windows.Forms.Button();
            this.cmdRunX = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownJogSpeed = new System.Windows.Forms.NumericUpDown();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusConnection = new System.Windows.Forms.ToolStripStatusLabel();
            this.SystemVoltage = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.cmdResetX = new System.Windows.Forms.Button();
            this.cmdResetY = new System.Windows.Forms.Button();
            this.txtXVal = new System.Windows.Forms.TextBox();
            this.txtYVal = new System.Windows.Forms.TextBox();
            this.cmdRUNAll = new System.Windows.Forms.Button();
            this.txtValX = new System.Windows.Forms.TextBox();
            this.txtValY = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBoxGOTO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStpsPermm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPosition)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJogSpeed)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bn_Conect
            // 
            this.bn_Conect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bn_Conect.Location = new System.Drawing.Point(-82, 32);
            this.bn_Conect.Margin = new System.Windows.Forms.Padding(4);
            this.bn_Conect.Name = "bn_Conect";
            this.bn_Conect.Size = new System.Drawing.Size(145, 44);
            this.bn_Conect.TabIndex = 0;
            this.bn_Conect.Text = "Connect";
            this.bn_Conect.UseVisualStyleBackColor = true;
            this.bn_Conect.Visible = false;
            this.bn_Conect.Click += new System.EventHandler(this.bn_Conect_Click);
            // 
            // buttonHome
            // 
            this.buttonHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonHome.Location = new System.Drawing.Point(71, 32);
            this.buttonHome.Margin = new System.Windows.Forms.Padding(4);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(125, 44);
            this.buttonHome.TabIndex = 1;
            this.buttonHome.Text = "HOME";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Visible = false;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // buttonGoto
            // 
            this.buttonGoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGoto.Location = new System.Drawing.Point(15, 23);
            this.buttonGoto.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGoto.Name = "buttonGoto";
            this.buttonGoto.Size = new System.Drawing.Size(100, 36);
            this.buttonGoto.TabIndex = 2;
            this.buttonGoto.Text = "RUN";
            this.buttonGoto.UseVisualStyleBackColor = true;
            this.buttonGoto.Click += new System.EventHandler(this.buttonGoto_Click);
            // 
            // groupBoxGOTO
            // 
            this.groupBoxGOTO.Controls.Add(this.RESET_POS);
            this.groupBoxGOTO.Controls.Add(this.label7);
            this.groupBoxGOTO.Controls.Add(this.label8);
            this.groupBoxGOTO.Controls.Add(this.label5);
            this.groupBoxGOTO.Controls.Add(this.labelPosition);
            this.groupBoxGOTO.Controls.Add(this.txtSetPos);
            this.groupBoxGOTO.Controls.Add(this.label1);
            this.groupBoxGOTO.Controls.Add(this.buttonGoto);
            this.groupBoxGOTO.Controls.Add(this.buttonHome);
            this.groupBoxGOTO.Controls.Add(this.bn_Conect);
            this.groupBoxGOTO.Controls.Add(this.buttonEnergize);
            this.groupBoxGOTO.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxGOTO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxGOTO.Location = new System.Drawing.Point(35, 161);
            this.groupBoxGOTO.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxGOTO.Name = "groupBoxGOTO";
            this.groupBoxGOTO.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxGOTO.Size = new System.Drawing.Size(579, 80);
            this.groupBoxGOTO.TabIndex = 3;
            this.groupBoxGOTO.TabStop = false;
            this.groupBoxGOTO.Visible = false;
            // 
            // RESET_POS
            // 
            this.RESET_POS.Enabled = false;
            this.RESET_POS.Location = new System.Drawing.Point(425, 20);
            this.RESET_POS.Margin = new System.Windows.Forms.Padding(4);
            this.RESET_POS.Name = "RESET_POS";
            this.RESET_POS.Size = new System.Drawing.Size(125, 47);
            this.RESET_POS.TabIndex = 2;
            this.RESET_POS.Text = "ZERO";
            this.RESET_POS.UseVisualStyleBackColor = true;
            this.RESET_POS.Click += new System.EventHandler(this.RESET_POS_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(329, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 25);
            this.label7.TabIndex = 11;
            this.label7.Text = "mm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(329, 46);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 25);
            this.label8.TabIndex = 12;
            this.label8.Text = "mm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(153, 16);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Position";
            // 
            // labelPosition
            // 
            this.labelPosition.BackColor = System.Drawing.SystemColors.Control;
            this.labelPosition.Location = new System.Drawing.Point(277, 17);
            this.labelPosition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.Size = new System.Drawing.Size(51, 27);
            this.labelPosition.TabIndex = 0;
            this.labelPosition.Text = "0.0 mm";
            this.labelPosition.Click += new System.EventHandler(this.labelPosition_Click);
            // 
            // txtSetPos
            // 
            this.txtSetPos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSetPos.Location = new System.Drawing.Point(283, 46);
            this.txtSetPos.Margin = new System.Windows.Forms.Padding(4);
            this.txtSetPos.Name = "txtSetPos";
            this.txtSetPos.Size = new System.Drawing.Size(39, 23);
            this.txtSetPos.TabIndex = 10;
            this.txtSetPos.Text = "0.0";
            this.txtSetPos.TextChanged += new System.EventHandler(this.txtSetPos_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Position set";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // buttonEnergize
            // 
            this.buttonEnergize.Enabled = false;
            this.buttonEnergize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEnergize.Location = new System.Drawing.Point(204, 32);
            this.buttonEnergize.Margin = new System.Windows.Forms.Padding(4);
            this.buttonEnergize.Name = "buttonEnergize";
            this.buttonEnergize.Size = new System.Drawing.Size(239, 44);
            this.buttonEnergize.TabIndex = 6;
            this.buttonEnergize.Text = "DE-ENERGIZE";
            this.buttonEnergize.UseVisualStyleBackColor = true;
            this.buttonEnergize.Visible = false;
            this.buttonEnergize.Click += new System.EventHandler(this.buttonEnergize_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(35, 304);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 38);
            this.button1.TabIndex = 11;
            this.button1.Text = "FILE SAVE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(259, 318);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "Steps / mm";
            this.label4.Visible = false;
            // 
            // nudStpsPermm
            // 
            this.nudStpsPermm.DecimalPlaces = 2;
            this.nudStpsPermm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudStpsPermm.Location = new System.Drawing.Point(387, 320);
            this.nudStpsPermm.Margin = new System.Windows.Forms.Padding(4);
            this.nudStpsPermm.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudStpsPermm.Minimum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.nudStpsPermm.Name = "nudStpsPermm";
            this.nudStpsPermm.Size = new System.Drawing.Size(100, 30);
            this.nudStpsPermm.TabIndex = 8;
            this.nudStpsPermm.Value = new decimal(new int[] {
            315,
            0,
            0,
            0});
            this.nudStpsPermm.Visible = false;
            this.nudStpsPermm.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(531, 327);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Speed";
            this.label2.Visible = false;
            // 
            // numericUpDownSpeed
            // 
            this.numericUpDownSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownSpeed.Increment = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownSpeed.Location = new System.Drawing.Point(621, 320);
            this.numericUpDownSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownSpeed.Maximum = new decimal(new int[] {
            50000000,
            0,
            0,
            0});
            this.numericUpDownSpeed.Name = "numericUpDownSpeed";
            this.numericUpDownSpeed.Size = new System.Drawing.Size(100, 30);
            this.numericUpDownSpeed.TabIndex = 5;
            this.numericUpDownSpeed.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownSpeed.Visible = false;
            this.numericUpDownSpeed.ValueChanged += new System.EventHandler(this.numericUpDownSpeed_ValueChanged);
            // 
            // chkReverse
            // 
            this.chkReverse.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReverse.AutoSize = true;
            this.chkReverse.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkReverse.Location = new System.Drawing.Point(20, 350);
            this.chkReverse.Margin = new System.Windows.Forms.Padding(4);
            this.chkReverse.Name = "chkReverse";
            this.chkReverse.Size = new System.Drawing.Size(125, 26);
            this.chkReverse.TabIndex = 9;
            this.chkReverse.Text = "Reverse Direction";
            this.chkReverse.UseVisualStyleBackColor = true;
            this.chkReverse.Visible = false;
            this.chkReverse.CheckedChanged += new System.EventHandler(this.chkReverse_CheckedChanged);
            // 
            // numericUpDownPosition
            // 
            this.numericUpDownPosition.DecimalPlaces = 2;
            this.numericUpDownPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownPosition.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericUpDownPosition.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownPosition.Location = new System.Drawing.Point(621, 281);
            this.numericUpDownPosition.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownPosition.Maximum = new decimal(new int[] {
            65,
            0,
            0,
            65536});
            this.numericUpDownPosition.Name = "numericUpDownPosition";
            this.numericUpDownPosition.Size = new System.Drawing.Size(100, 30);
            this.numericUpDownPosition.TabIndex = 4;
            this.numericUpDownPosition.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownPosition.Visible = false;
            this.numericUpDownPosition.ValueChanged += new System.EventHandler(this.numericUpDownPosition_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelVars);
            this.groupBox1.Controls.Add(this.labelStatus);
            this.groupBox1.Location = new System.Drawing.Point(20, 394);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(661, 320);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "STATUS";
            this.groupBox1.Visible = false;
            // 
            // labelVars
            // 
            this.labelVars.Location = new System.Drawing.Point(11, 22);
            this.labelVars.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVars.Name = "labelVars";
            this.labelVars.Size = new System.Drawing.Size(235, 489);
            this.labelVars.TabIndex = 1;
            this.labelVars.Text = "Vars";
            // 
            // labelStatus
            // 
            this.labelStatus.Location = new System.Drawing.Point(245, 20);
            this.labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(409, 196);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "Status";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.cmdJogUp);
            this.groupBox5.Controls.Add(this.cmdJog);
            this.groupBox5.Controls.Add(this.checkBoxJogRigth);
            this.groupBox5.Controls.Add(this.checkBoxJogLeft);
            this.groupBox5.Controls.Add(this.cmdRunY);
            this.groupBox5.Controls.Add(this.cmdRunX);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(13, 171);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(287, 80);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(115, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "JOG";
            // 
            // cmdJogUp
            // 
            this.cmdJogUp.Enabled = false;
            this.cmdJogUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJogUp.Location = new System.Drawing.Point(155, 28);
            this.cmdJogUp.Margin = new System.Windows.Forms.Padding(4);
            this.cmdJogUp.Name = "cmdJogUp";
            this.cmdJogUp.Size = new System.Drawing.Size(100, 38);
            this.cmdJogUp.TabIndex = 11;
            this.cmdJogUp.Text = "-->";
            this.cmdJogUp.UseVisualStyleBackColor = true;
            this.cmdJogUp.Click += new System.EventHandler(this.cmdJogUp_Click);
            this.cmdJogUp.MouseLeave += new System.EventHandler(this.cmdJogUp_MouseLeave);
            this.cmdJogUp.MouseHover += new System.EventHandler(this.cmdJogUp_MouseHover);
            // 
            // cmdJog
            // 
            this.cmdJog.Enabled = false;
            this.cmdJog.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJog.Location = new System.Drawing.Point(37, 28);
            this.cmdJog.Margin = new System.Windows.Forms.Padding(4);
            this.cmdJog.Name = "cmdJog";
            this.cmdJog.Size = new System.Drawing.Size(100, 38);
            this.cmdJog.TabIndex = 10;
            this.cmdJog.Text = "<--";
            this.cmdJog.UseVisualStyleBackColor = true;
            this.cmdJog.Click += new System.EventHandler(this.cmdJog_Click);
            this.cmdJog.MouseLeave += new System.EventHandler(this.cmdJog_MouseLeave);
            this.cmdJog.MouseHover += new System.EventHandler(this.cmdJog_MouseHover);
            // 
            // checkBoxJogRigth
            // 
            this.checkBoxJogRigth.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxJogRigth.Location = new System.Drawing.Point(155, 48);
            this.checkBoxJogRigth.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxJogRigth.Name = "checkBoxJogRigth";
            this.checkBoxJogRigth.Size = new System.Drawing.Size(52, 12);
            this.checkBoxJogRigth.TabIndex = 7;
            this.checkBoxJogRigth.Text = "--->";
            this.checkBoxJogRigth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxJogRigth.UseVisualStyleBackColor = true;
            this.checkBoxJogRigth.Visible = false;
            this.checkBoxJogRigth.CheckedChanged += new System.EventHandler(this.checkBoxJogRigth_CheckedChanged);
            // 
            // checkBoxJogLeft
            // 
            this.checkBoxJogLeft.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxJogLeft.Location = new System.Drawing.Point(37, 41);
            this.checkBoxJogLeft.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxJogLeft.Name = "checkBoxJogLeft";
            this.checkBoxJogLeft.Size = new System.Drawing.Size(63, 20);
            this.checkBoxJogLeft.TabIndex = 6;
            this.checkBoxJogLeft.Text = "<---";
            this.checkBoxJogLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxJogLeft.UseVisualStyleBackColor = true;
            this.checkBoxJogLeft.Visible = false;
            this.checkBoxJogLeft.CheckedChanged += new System.EventHandler(this.checkBoxJogLeft_CheckedChanged);
            // 
            // cmdRunY
            // 
            this.cmdRunY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRunY.Location = new System.Drawing.Point(145, 36);
            this.cmdRunY.Margin = new System.Windows.Forms.Padding(4);
            this.cmdRunY.Name = "cmdRunY";
            this.cmdRunY.Size = new System.Drawing.Size(100, 36);
            this.cmdRunY.TabIndex = 14;
            this.cmdRunY.Text = "RUN Y";
            this.cmdRunY.UseVisualStyleBackColor = true;
            this.cmdRunY.Visible = false;
            this.cmdRunY.Click += new System.EventHandler(this.cmdRunY_Click);
            // 
            // cmdRunX
            // 
            this.cmdRunX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRunX.Location = new System.Drawing.Point(37, 36);
            this.cmdRunX.Margin = new System.Windows.Forms.Padding(4);
            this.cmdRunX.Name = "cmdRunX";
            this.cmdRunX.Size = new System.Drawing.Size(100, 36);
            this.cmdRunX.TabIndex = 13;
            this.cmdRunX.Text = "RUN X";
            this.cmdRunX.UseVisualStyleBackColor = true;
            this.cmdRunX.Visible = false;
            this.cmdRunX.Click += new System.EventHandler(this.cmdRunX_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(671, 362);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Speed";
            this.label3.Visible = false;
            // 
            // numericUpDownJogSpeed
            // 
            this.numericUpDownJogSpeed.Location = new System.Drawing.Point(741, 358);
            this.numericUpDownJogSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownJogSpeed.Maximum = new decimal(new int[] {
            50000000,
            0,
            0,
            0});
            this.numericUpDownJogSpeed.Name = "numericUpDownJogSpeed";
            this.numericUpDownJogSpeed.Size = new System.Drawing.Size(100, 22);
            this.numericUpDownJogSpeed.TabIndex = 8;
            this.numericUpDownJogSpeed.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownJogSpeed.Visible = false;
            this.numericUpDownJogSpeed.ValueChanged += new System.EventHandler(this.numericUpDownJogSpeed_ValueChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Enabled = false;
            this.hScrollBar1.Location = new System.Drawing.Point(27, 356);
            this.hScrollBar1.Maximum = 5000;
            this.hScrollBar1.Minimum = -5000;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(628, 28);
            this.hScrollBar1.TabIndex = 7;
            this.hScrollBar1.Visible = false;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusConnection,
            this.SystemVoltage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 132);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(427, 26);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusConnection
            // 
            this.toolStripStatusConnection.Name = "toolStripStatusConnection";
            this.toolStripStatusConnection.Size = new System.Drawing.Size(116, 20);
            this.toolStripStatusConnection.Text = "Connection: OFF";
            this.toolStripStatusConnection.Visible = false;
            // 
            // SystemVoltage
            // 
            this.SystemVoltage.Name = "SystemVoltage";
            this.SystemVoltage.Size = new System.Drawing.Size(137, 20);
            this.SystemVoltage.Text = "System Voltage: 0.0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(427, 28);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 0;
            this.button2.Text = "Settings";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmdResetX
            // 
            this.cmdResetX.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdResetX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdResetX.Location = new System.Drawing.Point(281, 38);
            this.cmdResetX.Margin = new System.Windows.Forms.Padding(4);
            this.cmdResetX.Name = "cmdResetX";
            this.cmdResetX.Size = new System.Drawing.Size(129, 36);
            this.cmdResetX.TabIndex = 15;
            this.cmdResetX.Text = "RESET X";
            this.cmdResetX.UseVisualStyleBackColor = true;
            this.cmdResetX.Click += new System.EventHandler(this.cmdResetX_Click);
            // 
            // cmdResetY
            // 
            this.cmdResetY.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdResetY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdResetY.Location = new System.Drawing.Point(281, 85);
            this.cmdResetY.Margin = new System.Windows.Forms.Padding(4);
            this.cmdResetY.Name = "cmdResetY";
            this.cmdResetY.Size = new System.Drawing.Size(129, 36);
            this.cmdResetY.TabIndex = 16;
            this.cmdResetY.Text = "RESET Y";
            this.cmdResetY.UseVisualStyleBackColor = true;
            this.cmdResetY.Click += new System.EventHandler(this.cmdResetY_Click);
            // 
            // txtXVal
            // 
            this.txtXVal.Location = new System.Drawing.Point(160, 48);
            this.txtXVal.Margin = new System.Windows.Forms.Padding(4);
            this.txtXVal.Name = "txtXVal";
            this.txtXVal.Size = new System.Drawing.Size(49, 22);
            this.txtXVal.TabIndex = 17;
            // 
            // txtYVal
            // 
            this.txtYVal.Location = new System.Drawing.Point(160, 85);
            this.txtYVal.Margin = new System.Windows.Forms.Padding(4);
            this.txtYVal.Name = "txtYVal";
            this.txtYVal.Size = new System.Drawing.Size(49, 22);
            this.txtYVal.TabIndex = 18;
            // 
            // cmdRUNAll
            // 
            this.cmdRUNAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRUNAll.Location = new System.Drawing.Point(34, 62);
            this.cmdRUNAll.Margin = new System.Windows.Forms.Padding(4);
            this.cmdRUNAll.Name = "cmdRUNAll";
            this.cmdRUNAll.Size = new System.Drawing.Size(100, 36);
            this.cmdRUNAll.TabIndex = 19;
            this.cmdRUNAll.Text = "RUN";
            this.cmdRUNAll.UseVisualStyleBackColor = true;
            this.cmdRUNAll.Click += new System.EventHandler(this.cmdRUNAll_Click);
            // 
            // txtValX
            // 
            this.txtValX.Location = new System.Drawing.Point(217, 48);
            this.txtValX.Margin = new System.Windows.Forms.Padding(4);
            this.txtValX.Name = "txtValX";
            this.txtValX.Size = new System.Drawing.Size(49, 22);
            this.txtValX.TabIndex = 20;
            // 
            // txtValY
            // 
            this.txtValY.Location = new System.Drawing.Point(220, 85);
            this.txtValY.Margin = new System.Windows.Forms.Padding(4);
            this.txtValY.Name = "txtValY";
            this.txtValY.Size = new System.Drawing.Size(49, 22);
            this.txtValY.TabIndex = 21;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 158);
            this.Controls.Add(this.txtValY);
            this.Controls.Add(this.txtValX);
            this.Controls.Add(this.cmdRUNAll);
            this.Controls.Add(this.txtYVal);
            this.Controls.Add(this.txtXVal);
            this.Controls.Add(this.cmdResetY);
            this.Controls.Add(this.cmdResetX);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownJogSpeed);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.numericUpDownSpeed);
            this.Controls.Add(this.nudStpsPermm);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.numericUpDownPosition);
            this.Controls.Add(this.chkReverse);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxGOTO);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Stepper Motor Calibration Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxGOTO.ResumeLayout(false);
            this.groupBoxGOTO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStpsPermm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPosition)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJogSpeed)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bn_Conect;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.Button buttonGoto;
        private System.Windows.Forms.GroupBox groupBoxGOTO;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownSpeed;
        private System.Windows.Forms.NumericUpDown numericUpDownPosition;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox checkBoxJogRigth;
        private System.Windows.Forms.CheckBox checkBoxJogLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownJogSpeed;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelVars;
        private System.Windows.Forms.Button buttonEnergize;
        private System.Windows.Forms.Button RESET_POS;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudStpsPermm;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusConnection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelPosition;
        private System.Windows.Forms.CheckBox chkReverse;
        private System.Windows.Forms.Button cmdJog;
        private System.Windows.Forms.Button cmdJogUp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.TextBox txtSetPos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripStatusLabel SystemVoltage;
        private System.Windows.Forms.Button cmdRunX;
        private System.Windows.Forms.Button cmdRunY;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button cmdResetX;
        private System.Windows.Forms.Button cmdResetY;
        private System.Windows.Forms.TextBox txtXVal;
        private System.Windows.Forms.TextBox txtYVal;
        private System.Windows.Forms.Button cmdRUNAll;
        private System.Windows.Forms.TextBox txtValX;
        private System.Windows.Forms.TextBox txtValY;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

